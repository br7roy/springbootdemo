/*
* Package com.rust.springbootdemo.controller 
* FileName: DemoController
* Author:   Administrator
* Date:     2017/12/1 21:55
*/
package com.rust.springbootdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * FileName:    DemoController
 * Author:      Administrator
 * Date:        2017/12/1
 * Description:
 */
@Controller
public class DemoController extends BaseController {




    @RequestMapping("/first")
    public String main(){

        return "index";

    }



}
